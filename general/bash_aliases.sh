#! /bin/bash

# Notes:
#
# Use Ctrl-R to toggle history search and press it until you find what you need
#
#

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc). 
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi

stty -ixon # disable <C-s> freezing

function download_youtube_video {
  format_code=`youtube-dl -F "$1" | grep best | grep -E -o "^[0-9]*"`
  youtube-dl -f $format_code "$1"
}

source ${HOME}/.general_aliases.sh
source ${HOME}/.git_aliases.sh
source ${HOME}/.environment_vars.sh
