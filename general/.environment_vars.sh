function battery {
  if [ -f '/sys/class/power_supply/BAT0/capacity' ]; then
    echo `cat /sys/class/power_supply/BAT0/capacity`%
  else 
    echo ''
  fi
}

function is_vifm {
  if [ -n "$VIFM_SHELL" ]; then 
    echo _$VIFM_SHELL
  else
    echo ''
  fi
}

export CORES=$(cat /proc/cpuinfo | grep processor | wc -l)
export PS1="\e[94m\t \e[90m[${HOSTNAME}$(is_vifm)] \e[93m`battery` \e[92m\e[1m\w>\e[0m\e[39m \n# "
export TERM=xterm-256color
export EDITOR=vim
