#!/bin/bash

function setup_bash {
  DISTRO="$1"

  cat ${PWD}/bash_aliases.sh > ${HOME}/.bashrc
  echo "source \${HOME}/.${DISTRO}_aliases.sh" >> ${HOME}/.bashrc

  ln -sf ${PWD}/.general_aliases.sh ${HOME}/.general_aliases.sh
  ln -sf ${PWD}/../${DISTRO}/.${DISTRO}_aliases.sh ${HOME}/.${DISTRO}_aliases.sh
  ln -sf ${PWD}/.git_aliases.sh ${HOME}/.git_aliases.sh
  ln -sf ${PWD}/.environment_vars.sh ${HOME}/.environment_vars.sh
  ln -sf ${PWD}/.inputrc ${HOME}/.inputrc
}

function general_install {
  setup_bash $1
}

export -f general_install
