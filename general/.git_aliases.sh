alias agit="git add"
alias bgit='git branch'
alias cgit="git commit"
alias chgit='git checkout'
alias clgit='git clone'
alias dgit="git diff"
alias hgit='git log --oneline --name-status'
alias lgit='git log'
alias mgit='git merge'
alias pgit="git push"
alias plgit='git pull'
alias rbgit='git rebase'
alias rmgit='git remote -v'
alias rsgit='git reset'
alias sgit="git status"
alias shgit='git log --oneline'
alias stgit='git stash'
alias wgit='git whatchanged'   #git whatchanged -1 --format=oneline | wc -l
