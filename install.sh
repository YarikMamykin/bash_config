#! /bin/bash

[ -n "$DISTRO" ] || { echo "SPECIFY DISTRO ENV VAR!" && exit 1; }

pushd ${DISTRO}
  source ${DISTRO}_specific.sh
  specific_install
popd

pushd general
  source general_specific.sh
  general_install ${DISTRO}
popd 
