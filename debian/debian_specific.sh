#! /bin/bash


function specific_install {
  rm -fv ${HOME}/.profile
  ln -s ${PWD}/.profile ${HOME}/.profile
}

export -f specific_install 
