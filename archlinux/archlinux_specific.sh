#! /bin/bash

function specific_install {
  rm -fv ${HOME}/.profile
  ln -sf ${PWD}/.profile ${HOME}/.profile
}

export -f specific_install 
