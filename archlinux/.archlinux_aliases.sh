alias install="sudo pacman -Syy && sudo pacman -S"
alias remove="sudo pacman -R"
alias purge="sudo pacman -Rs"
alias search="sudo pacman -Ss"
alias update="sudo pacman -Syy"
alias upgrade="sudo pacman -Syu"


function install_aur_package {
  local package_name="$1"
  pushd ${PWD}
  cd /tmp
  git clone https://aur.archlinux.org/${package_name}.git
  cd ${package_name}
  makepkg -si --noconfirm
  popd
}

